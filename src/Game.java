import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.util.Random;

import javax.swing.ImageIcon;

public class Game {

    ActionHandler aHandler = new ActionHandler(this);
    UI ui = new UI(this);
    Cards cards = new Cards();
    Random random = new Random();
    static Player player1 = new Player();

    int pickedCardNum;
    int playerHas = 0;
    int dealerHas = 0;
    int[] playerCardNum = new int[6];
    int[] dealerCardNum = new int[6];
    int[] playerCardValue = new int[6];
    int[] dealerCardValue = new int[6];
    int playerTotalValue;
    int dealerTotalValue;

    // OTHERS
    String situation = "";
    ImageIcon dealerSecondCard;

    public static void main(String[] args) {
        new Game();
    }

    public void createPlayer(String name, double money) {

        player1 = new Player(name, money);
        createPlayer();
    }
    public void createPlayer() {

        ui.window.setVisible(false);
        ui.startBtn.setVisible(false);
        ui.historyBtn.setVisible(false);
        ui.exitBtn.setVisible(false);
        ui.moneyAndConsent.setVisible(true);
    }
    public static double playerBet(double amount){
        player1.setBetAmount(amount);
        return amount;
    }

        public void titleToGame() {

        ui.moneyAndConsent.setVisible(false);
        ui.secondScreen.setVisible(true);
        startGame();
    }
    public void gameToTitle() {

        ui.secondScreen.setVisible(false);
        ui.window.setVisible(true);
        ui.startBtn.setVisible(true);
        ui.historyBtn.setVisible(true);
        ui.exitBtn.setVisible(true);

    }
    public void startGame() {

        ui.hideCards.setVisible(true);
        ui.hidePlayerScore.setVisible(true);
        ui.hideTextBtn.setVisible(true);
        ui.moneyToBet.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                ui.hideCards.setVisible(false);
                ui.hidePlayerScore.setVisible(false);
                ui.hideTextBtn.setVisible(false);
            }
        });
        dealerDraw();
        playerDraw();
        dealerDraw();
        ui.dealerCardLabel[2].setIcon(cards.front);
        ui.dealerScore.setText("Dealer: ?");
        ui.amountText.setText("You have: " + player1.getMoney() + "$");
        playerTurn();
    }
    public void dealerDraw() {

        dealerHas++;

        ImageIcon pickedCard = pickRandomCard();
        Image image = pickedCard.getImage();
        Image newImg = image.getScaledInstance((pickedCard.getIconWidth() / 5 ) + 5, (pickedCard.getIconHeight() / 5 ) + 5, java.awt.Image.SCALE_SMOOTH);
        pickedCard = new ImageIcon(newImg);

        if(dealerHas==2) {
            dealerSecondCard = pickedCard;
        }

        dealerCardNum[dealerHas] = pickedCardNum;
        dealerCardValue[dealerHas] = checkCardValue();

        ui.dealerCardLabel[dealerHas].setVisible(true);
        ui.dealerCardLabel[dealerHas].setIcon(pickedCard);

        dealerTotalValue = dealerTotalValue();
        ui.dealerScore.setText("Dealer: " + dealerTotalValue);
    }
    public void playerDraw() {

        playerHas++;

        ImageIcon pickedCard = pickRandomCard();
        Image image = pickedCard.getImage();
        Image newImg = image.getScaledInstance((pickedCard.getIconWidth() / 5 ) + 5, (pickedCard.getIconHeight() / 5 ) + 5, java.awt.Image.SCALE_SMOOTH);
        pickedCard = new ImageIcon(newImg);

        playerCardNum[playerHas] = pickedCardNum;
        playerCardValue[playerHas] = checkCardValue();

        ui.playerCardLabel[playerHas].setVisible(true);
        ui.playerCardLabel[playerHas].setIcon(pickedCard);

        playerTotalValue = playerTotalValue();
        ui.playerScore.setText("You: " + playerTotalValue);
    }
    public void playerTurn() {

        situation = "playerTurn";

        playerDraw();

        if(playerTotalValue > 21) {
            dealerOpen();
        }
        else if(playerTotalValue == 21 && playerHas == 2) {
            playerNatural();
        }
        else {
            if(playerHas > 1 && playerHas < 5) {
                ui.messageText.setText("Do you want to hit one more card?");
                ui.button[1].setVisible(true);
                ui.button[1].setText("Hit");
                ui.button[2].setVisible(true);
                ui.button[2].setText("Stand");
            }
            if(playerHas == 5) {
                dealerOpen();
            }
        }
    }
    public void playerNatural() {

        situation = "playerNatural";
        player1.setMoney((player1.getMoney() - player1.getBetAmount()) + (player1.getBetAmount() * 2.5));
        ui.messageText.setText("You got natural. Let's open the dealer's card.");
        ui.button[1].setVisible(true);
        ui.button[1].setText("Continue");
    }
    public void dealerOpen() {

        ui.dealerCardLabel[2].setIcon(dealerSecondCard);
        ui.dealerScore.setText("Dealer: " + dealerTotalValue);

        if(playerHas == 2 && playerTotalValue == 21) {
            checkResult();
        }
        else if(dealerTotalValue < 17 && playerTotalValue <= 21) {
            dealerTurnContinue();
        }
        else {
            checkResult();
        }
    }
    public void dealerTurn() {

        if(dealerTotalValue < 17) {

            dealerDraw();

            if(dealerHas == 5 || dealerTotalValue >= 17) {
                checkResult();
            }
            else {
                dealerTurnContinue();
            }
        }
        else {
            checkResult();
        }
    }
    public void dealerTurnContinue() {

        situation = "dealerTurnContinue";

        ui.messageText.setText("The dealer is going to hit another card.");
        ui.button[1].setVisible(true);
        ui.button[1].setText("Continue");
    }
    public void checkResult() {

        situation = "checkResult";

        if(playerTotalValue > 21) {
            ui.messageText.setText("You lost!");
            player1.setMoney(player1.getMoney() - (player1.getBetAmount()));
            gameFinished();
        }
        else {
            if(playerTotalValue == 21 && dealerHas == 2) {
                if(dealerTotalValue == 21) {
                    ui.messageText.setText("Draw!");
                    gameFinished();
                }
                else {
                    ui.messageText.setText("You won!");
                    player1.setMoney((player1.getMoney() - player1.getBetAmount()) + (player1.getBetAmount() * 2));
                    gameFinished();
                }
            }
            else {
                if(dealerTotalValue < 22 && dealerTotalValue > playerTotalValue) {
                    ui.messageText.setText("You lost!");
                    player1.setMoney(player1.getMoney() - (player1.getBetAmount()));
                    gameFinished();
                }
                else if(dealerTotalValue == playerTotalValue) {
                    ui.messageText.setText("Draw!");
                    gameFinished();
                }
                else {
                    ui.messageText.setText("You won!");
                    player1.setMoney((player1.getMoney() - player1.getBetAmount()) + (player1.getBetAmount() * 2));
                    gameFinished();
                }
            }
        }
    }
    public void gameFinished() {

        situation = "gameFinished";
        ui.button[1].setVisible(true);
        ui.button[1].setText("Play Again");
        ui.button[2].setVisible(true);
        ui.button[2].setText("Leave Table");
    }
    public void resetEverything() {

        for(int i=1; i < 6; i++ ){
            ui.playerCardLabel[i].setVisible(false);
            ui.dealerCardLabel[i].setVisible(false);
        }
        for(int i=1; i < 6; i++) {
            playerCardNum[i]=0;
            playerCardValue[i]=0;
            dealerCardNum[i]=0;
            dealerCardValue[i]=0;
        }
        playerHas=0;
        dealerHas=0;

        removeButtons();
        startGame();
    }
    public int playerTotalValue() {

        playerTotalValue = playerCardValue[1] + playerCardValue[2] + playerCardValue[3] + playerCardValue[4] + playerCardValue[5];

        if(playerTotalValue > 21) {
            adjustPlayerAceValue();
        }
        playerTotalValue = playerCardValue[1] + playerCardValue[2] + playerCardValue[3] + playerCardValue[4] + playerCardValue[5];
        return playerTotalValue;
    }
    public int dealerTotalValue() {

        dealerTotalValue = dealerCardValue[1] + dealerCardValue[2] + dealerCardValue[3] + dealerCardValue[4] + dealerCardValue[5];

        if(dealerTotalValue > 21) {
            adjustDealerAceValue();
        }
        dealerTotalValue = dealerCardValue[1] + dealerCardValue[2] + dealerCardValue[3] + dealerCardValue[4] + dealerCardValue[5];
        return dealerTotalValue;
    }
    public void adjustPlayerAceValue() {

        for(int i=1; i<6; i++) {
            if(playerCardNum[i]==1) {
                playerCardValue[i]=1;
                playerTotalValue = playerCardValue[1] + playerCardValue[2] + playerCardValue[3] + playerCardValue[4] + playerCardValue[5];
                if(playerTotalValue < 21) {
                    break;
                }
            }
        }
    }
    public void adjustDealerAceValue() {

        for(int i=1; i<6; i++) {
            if(dealerCardNum[i]==1) {
                dealerCardValue[i]=1;
                dealerTotalValue = dealerCardValue[1] + dealerCardValue[2] + dealerCardValue[3] + dealerCardValue[4] + dealerCardValue[5];
                if(dealerTotalValue < 21) {
                    break;
                }
            }
        }
    }

    public ImageIcon pickRandomCard() {

        ImageIcon pickedCard = null;

        pickedCardNum = random.nextInt(13)+1;
        int pickedMark = random.nextInt(4)+1;

        switch(pickedMark) {
            case 1: pickedCard = cards.spade[pickedCardNum]; break;
            case 2: pickedCard = cards.heart[pickedCardNum]; break;
            case 3: pickedCard = cards.club[pickedCardNum]; break;
            case 4: pickedCard = cards.diamond[pickedCardNum]; break;
        }
        return pickedCard;
    }
    public int checkCardValue() {

        int cardValue = pickedCardNum;
        if(pickedCardNum==1) {
            cardValue=11;
        }
        if(pickedCardNum>10) {
            cardValue=10;
        }
        return cardValue;
    }
    public void removeButtons() {

        ui.button[1].setVisible(false);
        ui.button[2].setVisible(false);
        ui.button[3].setVisible(false);
        ui.button[4].setVisible(false);
        ui.button[5].setVisible(false);
    }

}
