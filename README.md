# BlackJack - Java Swing UI Game

Java Swing is a Java library for UI implementation.

A game of blackjack is dealt from a standard 52-card deck, and anywhere from 1 to 8 decks may be used by the casino. No matter what variation is being played, the objective is always the same: to get a higher score than the dealer without exceeding a total of 21. If the player goes over this amount, they have busted and automatically lose.
Here is a breakdown of the various card values in blackjack:
Card	Value
2 – 10	Face Value in Points
King, Queen, or Jack	Ten Points
Ace	One or Eleven Points
If the player holds an ace and a 10-point card, they are said to have a “blackjack.” This is the best possible hand for a player. In most games, a blackjack pays out at 3:2 odds, while any other win results in an even money payout. In the case of a tie, the result is called a “push” and no money changes hands.Players begin the game by placing their wager. Each casino table has minimum and maximum betting requirements, so be sure to learn this information before starting.Once the initial wagers have been made, everyone at the table receives two cards. The player cards are dealt facing upwards, while the dealer receives one down (hole card) and one up (upcard). The main exception to this rule comes in the European version of blackjack, which requires the dealer to wait until players have finished their hands before drawing their second card.If the dealer’s upcard is an ace, then the player has the option of buying insurance against a dealer blackjack if the casino rules allow it. In this scenario, the player is allowed to make an additional bet up to half the size of their original wager. If the house does have a blackjack, then the insurance bet pays out at 2:1 and the hand is over. Otherwise, the bet is lost to the casino and the hand continues.If the dealer’s upcard is worth 10 points, he immediately checks his hole card for a possible blackjack. If this is the case, then he gives everyone the bad news and collects all wagers. The only exception occurs when a player also has a natural blackjack, which results in a “push” (or tie).
## GamePlay

![BlackJack Gif](BlackJack.gif)

## Installation

WINDOLS
Use the git clone method https://git.trading212.io/hristos.dimitrov/blackjackgame-byhrisd.git/BlackJack.zip to download BlackJack.
After that extract the zip with appropriate windows tool.
MacOS

## Usage

Start the .exe file (keep all the files in one directory, so everything runs smootly).

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Project status
BlackJack is up and running on a singleplayer mode in the time being. In the future we will improve the graphics, include a way to pay with credit/debit card and play with other people online.

## Name
BlackJack

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.
-Hristos Dimitrov

## License
[MIT](https://choosealicense.com/licenses/mit/)
