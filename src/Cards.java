import javax.swing.*;
import java.awt.*;

public class Cards {

    ImageIcon front = new ImageIcon();
    ImageIcon[] spade = new ImageIcon[14];
    ImageIcon[] heart = new ImageIcon[14];
    ImageIcon[] club = new ImageIcon[14];
    ImageIcon[] diamond = new ImageIcon[14];

    public Cards() {

        front = new ImageIcon(String.valueOf(new ImageIcon("Images/defaultCard.png")));
        Image image = front.getImage();
        Image newImg = image.getScaledInstance((front.getIconWidth() / 5 ) + 5, (front.getIconHeight() / 5 ) + 5, java.awt.Image.SCALE_SMOOTH);
        front = new ImageIcon(newImg);

        // SPADE
        for(int num=1; num<14; num++) {
            spade[num]  = new ImageIcon(String.valueOf(new ImageIcon("Images/S/" + num + "S.png")));
        }

        // HEART
        for(int num=1; num<14; num++) {
            heart[num]  = new ImageIcon(String.valueOf(new ImageIcon("Images/H/" + num + "H.png")));
        }

        // CLUB
        for(int num=1; num<14; num++) {
            club[num]  = new ImageIcon(String.valueOf(new ImageIcon("Images/C/" + num + "C.png")));
        }

        // DIAMOND
        for(int num=1; num<14; num++) {
            diamond[num]  = new ImageIcon(String.valueOf(new ImageIcon("Images/D/" + num + "D.png")));
        }
    }
}