import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class UI extends JFrame{
    Game game;

    // Intro Screen UI
    JFrame window;
    public JTextArea messageText;
    public JTextArea amountText;
    public JButton startBtn;
    public JButton historyBtn;
    public JButton exitBtn;
    public JPanel introFrame;

    // Money and Consent UI
    JFrame moneyAndConsent;

    public JTextArea consentText;

    public JTextArea nameText;
    public JTextField name;

    public JTextArea moneyText;
    public JTextField money;

    public JButton continueBtn;

    // Table UI
    JPanel table;
    JPanel hideCards;
    JPanel hidePlayerScore;
    JPanel hideTextBtn;
    JFrame secondScreen;
    JPanel dealerPanel;
    JPanel playerPanel;

    public JTextArea moneyToBetText;
    public JTextField moneyToBet;
    JLabel[] playerCardLabel = new JLabel[6];
    JLabel[] dealerCardLabel = new JLabel[6];

    // Message UI
    JPanel scorePanel;
    JLabel playerScore, dealerScore;
    JPanel buttonPanel = new JPanel();
    JButton[] button = new JButton[6];

    int cardWidth = 150;
    int cardHeight = 213;

    public UI(Game game) {
        this.game = game;

        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        createMainField();
        createTableUI();
        createOtherUI();
        createMoneyAndConsentUI();
        window.setVisible(true);
    }

    public void createMainField(){

        window = new JFrame();
        window.setSize(800,600);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.getContentPane().setBackground(Color.black);
        window.setTitle("BlackJack by Hris");
        window.setLayout(null);
        window.setLocationRelativeTo(null);


        messageText = new JTextArea("Hello there! Welcome to the best BlackJack game ever created! Press Start to begin the game!");
        messageText.setBounds(5,30,780,100);
        messageText.setBackground(Color.black);
        messageText.setForeground(Color.white);
        messageText.setEditable(false);
        messageText.setLineWrap(true);
        messageText.setWrapStyleWord(true);
        messageText.setFont(new Font("Book Antiqua", Font.PLAIN, 26));
        window.add(messageText);

        startBtn  = new JButton("Start");
        startBtn.setFont(new Font("Gill Sans MT",Font.BOLD,21));
        startBtn.setOpaque(true);
        startBtn.setBounds(300,390,130,50);
        startBtn.setForeground(Color.black);
        startBtn.addActionListener(game.aHandler);
        window.add(startBtn);

        historyBtn = new JButton("History");
        historyBtn.setFont(new Font("Gill Sans MT",Font.BOLD,19));
        historyBtn.setOpaque(true);
        historyBtn.setBounds(300,445,130,50);
        historyBtn.setForeground(Color.black);
        window.add(historyBtn);

        exitBtn  = new JButton("Exit");
        exitBtn.setFont(new Font("Gill Sans MT",Font.BOLD,21));
        exitBtn.setOpaque(true);
        exitBtn.setBounds(300,500,130,50);
        exitBtn.setForeground(Color.black);
        exitBtn.addActionListener(game.aHandler);
        window.add(exitBtn);

        introFrame = new JPanel();
        introFrame.setBounds(5, 58, 777,  500);
        introFrame.setVisible(true);
        introFrame.add(new JLabel(new ImageIcon("Images/BlackjackIntroPic.jpg")));
        window.add(introFrame);
    }

    public void createMoneyAndConsentUI(){

        moneyAndConsent = new JFrame();
        moneyAndConsent.getContentPane().setBackground(new Color(0,81,0));
        moneyAndConsent.setSize(800,600);
        moneyAndConsent.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        moneyAndConsent.setTitle("Consent for Gaming!");
        moneyAndConsent.setLayout(null);
        moneyAndConsent.setLocationRelativeTo(null);

        consentText = new JTextArea("Blackjack, or Twenty-one, Card game whose object is to be dealt cards having a higher count than those of the dealer, up to but not exceeding 21. The dealer may use a single deck of 52 cards or two or more decks from a holder called a shoe. Aces count as 1 or 11, and face cards as 10. Depending on the rules used, bets may be placed before the deal, after each player has been dealt one card facedown, or after each player has received two cards facedown and the dealer has exposed one of his cards.");
        consentText.setBounds(5,30,780,100);
        consentText.setBackground(new Color(0,81,0));
        consentText.setForeground(Color.white);
        consentText.setEditable(false);
        consentText.setLineWrap(true);
        consentText.setWrapStyleWord(true);
        consentText.setFont(new Font("Book Antiqua", Font.PLAIN, 26));
        moneyAndConsent.add(consentText);

        nameText = new JTextArea("Enter your name:");
        nameText.setBounds(15,200,300,50);
        nameText.setBackground(new Color(0,81,0));
        nameText.setForeground(Color.white);
        nameText.setEditable(false);
        nameText.setLineWrap(true);
        nameText.setWrapStyleWord(true);
        nameText.setFont(new Font("Book Antiqua", Font.PLAIN, 23));
        moneyAndConsent.add(nameText);
        name = new JTextField();
        name.setBackground(Color.white);
        name.setBounds(220,200,300,50);
        name.setLayout(null);
        moneyAndConsent.add(name);

        moneyText = new JTextArea("Enter money amount ex: (20)");
        moneyText.setBounds(15,255,200,100);
        moneyText.setBackground(new Color(0,81,0));
        moneyText.setForeground(Color.white);
        moneyText.setEditable(false);
        moneyText.setLineWrap(true);
        moneyText.setWrapStyleWord(true);
        moneyText.setFont(new Font("Book Antiqua", Font.PLAIN, 23));
        moneyAndConsent.add(moneyText);
        money = new JTextField();
        money.setBackground(Color.white);
        money.setBounds(220,260,300,50);
        money.setLayout(null);
        moneyAndConsent.add(money);

        continueBtn  = new JButton("Continue");
        continueBtn.setFont(new Font("Gill Sans MT",Font.BOLD,21));
        continueBtn.setOpaque(true);
        continueBtn.setBounds(300,500,130,50);
        continueBtn.setForeground(Color.black);
//        continueBtn.addActionListener(game.aHandler);
        continueBtn.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                String getName = name.getText();
                double getMoney = Double.parseDouble(money.getText());
                game.createPlayer(getName, getMoney);
                game.titleToGame();

            }
        });
        moneyAndConsent.add(continueBtn) ;
    }

    public void createTableUI() {

        secondScreen = new JFrame();
        secondScreen.setTitle("Best BlackJack!");
        secondScreen.setSize(1200,700);
        secondScreen.setLocationRelativeTo(null);
        secondScreen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        table = new JPanel();
        table.setBackground(new Color(0,81,0));
        table.setBounds(50,50,850,600);
        table.setLayout(null);
        table.setVisible(true);

        hideCards = new JPanel();
        hideCards.setBackground(new Color(0,81,0));
        hideCards.setBounds(5,5,400,580);
        hideCards.setLayout(null);
        hideCards.setVisible(true);
        table.add(hideCards);
        secondScreen.add(hideCards);
        hidePlayerScore = new JPanel();
        hidePlayerScore.setBackground(new Color(0,81,0));
        hidePlayerScore.setBounds(50,590,150,50);
        hidePlayerScore.setLayout(null);
        hidePlayerScore.setVisible(true);
        table.add(hidePlayerScore);
        secondScreen.add(hidePlayerScore);
        hideTextBtn = new JPanel();
        hideTextBtn.setBackground(new Color(0,81,0));
        hideTextBtn.setBounds(400,500,1000,200);
        hideTextBtn.setLayout(null);
        hideTextBtn.setVisible(true);
        table.add(hideTextBtn);
        secondScreen.add(hideTextBtn);


        dealerPanel = new JPanel();
        dealerPanel.setBounds(100,120,cardWidth*5,cardHeight);
        dealerPanel.setBackground(null);
        dealerPanel.setOpaque(false);
        dealerPanel.setLayout(new GridLayout(1,5));
        dealerPanel.setVisible(true);
        table.add(dealerPanel);
        secondScreen.add(dealerPanel);

        playerPanel = new JPanel();
        playerPanel.setBounds(100,370,cardWidth*5,cardHeight);
        playerPanel.setOpaque(false);
        playerPanel.setLayout(new GridLayout(1,5));
        playerPanel.setVisible(true);
        table.add(playerPanel);
        secondScreen.add(playerPanel);

        for(int i = 1; i < 6; i++) {
            playerCardLabel[i] = new JLabel();
            playerCardLabel[i].setVisible(true);
            playerPanel.add(playerCardLabel[i]);
        }
        for(int i = 1; i < 6; i++) {
            dealerCardLabel[i] = new JLabel();
            dealerCardLabel[i].setVisible(true);
            dealerPanel.add(dealerCardLabel[i]);
        }

        dealerScore = new JLabel();
        dealerScore.setBounds(50,10,200,50);
        dealerScore.setForeground(Color.white);
        dealerScore.setFont(new Font("Times New Roman", Font.PLAIN, 42));
        table.add(dealerScore);
        secondScreen.add(dealerScore);

        playerScore = new JLabel();
        playerScore.setBounds(50,590,200,50);
        playerScore.setForeground(Color.white);
        playerScore.setFont(new Font("Times New Roman", Font.PLAIN, 42));
        table.add(playerScore);
        secondScreen.add(playerScore);

        moneyToBetText = new JTextArea("Enter BET below:");
        moneyToBetText.setBounds(220,580,200,30);
        moneyToBetText.setBackground(new Color(0,81,0));
        moneyToBetText.setForeground(Color.white);
        moneyToBetText.setEditable(false);
        moneyToBetText.setLineWrap(true);
        moneyToBetText.setWrapStyleWord(true);
        moneyToBetText.setFont(new Font("Book Antiqua", Font.PLAIN, 18));
        table.add(moneyToBetText);
        secondScreen.add(moneyToBetText);
        moneyToBet = new JTextField();
        moneyToBet.setBackground(Color.white);
        moneyToBet.setBounds(240,620,100,30);
        moneyToBet.setLayout(null);
//        moneyToBet.addKeyListener(new KeyListener() {
//            @Override
//            public void keyTyped(KeyEvent e) {
//
//            }
//
//            @Override
//            public void keyPressed(KeyEvent e) {
//
//            }
//
//            @Override
//            public void keyReleased(KeyEvent e) {
//                double getMoneyBet = Double.parseDouble(moneyToBet.getText());
//                System.out.println(getMoneyBet);
//            }
//        });
        moneyToBet.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                double getMoneyBet = Double.parseDouble(moneyToBet.getText());
                System.out.println(getMoneyBet);
                moneyToBet.setText("");
                hideCards.setVisible(false);
                hideTextBtn.setVisible(false);
                hidePlayerScore.setVisible(false);
                Game.playerBet(getMoneyBet);
            }
        });
        table.add(moneyToBet);
        secondScreen.add(moneyToBet);

        secondScreen.add(table);
    }
    public void createOtherUI() {

        messageText = new JTextArea();
        messageText.setBounds(430,590,720,100);
        messageText.setBackground(null);
        messageText.setForeground(Color.white);
        messageText.setFont(new Font("Times New Roman", Font.PLAIN, 40));
        messageText.setEditable(false);
        table.add(messageText);

        amountText = new JTextArea();
        amountText.setBounds(430,10,720,100);
        amountText.setBackground(null);
        amountText.setForeground(Color.white);
        amountText.setFont(new Font("Times New Roman", Font.PLAIN, 40));
        amountText.setEditable(false);
        table.add(amountText);

        buttonPanel = new JPanel();
        buttonPanel.setBounds(920,340,200,300);
        buttonPanel.setBackground(null);
        buttonPanel.setLayout(new GridLayout(6,1));
        table.add(buttonPanel);

        for(int i = 1; i < 6; i++) {
            button[i] = new JButton();
            button[i].setBackground(null);
            button[i].setForeground(Color.white);
            button[i].setFocusPainted(false);
            button[i].setBorder(null);
            button[i].setFont(new Font("Times New Roman", Font.PLAIN, 42));
            button[i].addActionListener(game.aHandler);
            button[i].setActionCommand(""+i);
            button[i].setVisible(false);
            buttonPanel.add(button[i]);
        }
    }
}
